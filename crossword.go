package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strings"
	"sync"
)

// Might need to be boolean channels
var wordFound = false
var across bool
var diagonal bool
var down bool
var direction = ""
var mutex = &sync.Mutex{}

// Should be in a method
func findWord(word string, grid [][]string, data chan string, wg *sync.WaitGroup) {
	letters := strings.Split(word, "")
	var position = 0
	var matchFound = false
	var direction string
	var columnMap = make(map[int]int)

	println("Searching for: " + word)
	for column := 0; column < len(grid[0]); column++ {
		columnMap[column] = column
	}

	wordFound = false
	for row := 0; row < len(grid); row++ {
		var column int

		for column < len(grid[0]) {
			if letters[position] == grid[row][columnMap[column]] {
				matchFound, direction = searchForWord(position, letters, grid, row, column)
				if matchFound {
					break
				}
			}
			column++
		}
		if matchFound {
			break
		}
	}

	if matchFound {
		data <- word + " was FOUND " + direction

	} else {
		data <- word + " was NOT FOUND"
	}
	wg.Done()
}

func printWord(data chan string, done chan bool) {
	for d := range data {
		fmt.Println(d)
	}
	done <- true
}

func searchForWord(position int, letters []string, grid [][]string, currentRow int, currentColumn int) (bool, string) {
	if letters[position] != grid[currentRow][currentColumn] {
		direction = ""
		wordFound = false
		return false, direction
	}

	if position == len(letters)-1 {
		wordFound = true
		if across {
			direction = "across"
		} else if diagonal {
			direction = "diagonal"
		} else if down {
			direction = "down"
		} else {
			fmt.Println("Should never reach here!")
			return false, direction
		}
		return true, direction
	}

	position++

	// check right if column < len(grid)
	if !wordFound && currentColumn < len(grid)-1 {
		direction = "across"
		mutex.Lock()
		across = true
		diagonal = false
		down = false
		mutex.Unlock()
		searchForWord(position, letters, grid, currentRow, currentColumn+1)
	}

	// check down right if column < len(grid)
	if !wordFound && currentColumn < len(grid)-1 && currentRow < len(grid)-1 {
		direction = "diagonal"
		mutex.Lock()
		across = false
		diagonal = true
		down = false
		mutex.Unlock()
		searchForWord(position, letters, grid, currentRow+1, currentColumn+1)
	}

	// check down if row < len(grid)
	if !wordFound && currentRow < len(grid)-1 {
		direction = "down"
		mutex.Lock()
		across = false
		diagonal = false
		down = true
		mutex.Unlock()
		searchForWord(position, letters, grid, currentRow+1, currentColumn)
	}

	position--
	return wordFound, direction
}

func setupGrid(fileGrid *os.File) ([][]string, error) {
	var grid [][]string
	scannerGrid := bufio.NewScanner(fileGrid)
	for scannerGrid.Scan() {
		var text string = scannerGrid.Text()
		text = strings.ReplaceAll(text, ",", "")
		var gridRow []string
		for _, v := range text {
			gridRow = append(gridRow, string(v))
		}

		grid = append(grid, gridRow)
		gridRow = nil
	}

	if errGrid := scannerGrid.Err(); errGrid != nil {
		return nil, errGrid
	}
	return grid, nil
}

func run(dictionaryPtr *string, gridPtr *string) error {
	fileDictionary, err := os.Open(*dictionaryPtr)
	if err != nil {
		return err
	}
	defer fileDictionary.Close()

	fileGrid, err := os.Open(*gridPtr)
	if err != nil {
		return err
	}
	defer fileGrid.Close()

	grid, err := setupGrid(fileGrid)
	if err != nil {
		return err
	}

	data := make(chan string)
	done := make(chan bool)
	wg := sync.WaitGroup{}

	across = false
	diagonal = false
	down = false

	scannerDictionary := bufio.NewScanner(fileDictionary)
	for scannerDictionary.Scan() {
		wg.Add(1)
		go findWord(scannerDictionary.Text(), grid, data, &wg)
	}

	go printWord(data, done)

	go func() {
		wg.Wait()
		close(data)
	}()

	<-done

	if errDictionary := scannerDictionary.Err(); errDictionary != nil {
		return errDictionary
	}

	return nil
}

func main() {
	dictionaryPtr := flag.String("dictionary", "", "a dictionary file")
	gridPtr := flag.String("grid", "", "a grid file")

	flag.Parse()

	if err := run(dictionaryPtr, gridPtr); err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}
}
